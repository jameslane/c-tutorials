#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int anotherGame(void) {
  char answer;
  do {
    printf("Do you want to play another game? [y/n]:\n");
    answer = getchar();
  } while (!(answer == 'y' || answer == 'n'));
  return answer == 'y';
}

void readGuess(int *guess, size_t size) {
  printf("Enter your guess:\n");
  int bufferLength = 100;
  char buffer[bufferLength];
  int bufferSize = sizeof(buffer);
  fgets(buffer, bufferSize, stdin);
  for (int i = 0; i < size; i++) {
    do {
      guess[i] = buffer[i] - '0';
    } while (guess[i] < 1 || guess[i] > 9);
  }
}

int blackScore(int guess[], int code[], size_t size) {
  int score = 0;
  for (int i = 0; i < size; i++) {
    if (code[i] == guess[i]) {
      score++;
    }
  }
  return score;
}

int whiteScore(int guess[], int code[], size_t size) {
  int score = 0;
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      if (i != j && code[i] == guess[j]) {
        score++;
      }
    }
  }
  return score;
}

void printScore(int g[], int c[], size_t size) {
  int b = blackScore(g, c, size);
  int w = whiteScore(g, c, size);
  printf("(%i, %i)\n", b, w);
}

int main(int argc, char **argv) {
  int numCodes = 5;
  size_t size = 4;
  int codes[5][4] = {{1, 8, 9, 2}, {2, 4, 6, 8}, {1, 9, 8, 3},
    {7, 4, 2, 1}, {4, 6, 8, 9}};

  int *guess = malloc(size * sizeof(int));
  for (int i = 0; i < numCodes; i++) {
    readGuess(guess, size);
    while (blackScore(guess, codes[i], size) != size) {
      printScore(guess, codes[i], size);
      readGuess(guess, size);
    }

    printf("You have guessed correctly!\n");
    if (i < numCodes - 1) {
      int another = anotherGame();
      if (!another) {
        break;
      }
    }
  }
  free(guess);
}
