#include <stdint.h>
#include <stdio.h>

void printBits(uint32_t x) { 
  int i;
  uint32_t mask = 1 << 31;
  for (i = 0; i < 32; i++) { 
    if (i == 16) {
      printf(" ");
    }
    if ((x & mask) == 0) {
      printf("0");
    } else {
      printf("1"); 
    }
    x = x << 1; 
  }
  printf("\n");
}

void printBits16(uint16_t x) { 
  int i;
  uint16_t mask = 1 << 15;
  for (i = 0; i < 16; i++) { 
    if ((x & mask) == 0) {
      printf("0");
    } else {
      printf("1"); 
    }
    x = x << 1; 
  }
  printf("\n");
}

void printSBits16(int16_t x) { 
  int i;
  int16_t mask = 1 << 15;
  for (i = 0; i < 16; i++) { 
    if ((x & mask) == 0) {
      printf("0");
    } else {
      printf("1"); 
    }
    x = x << 1; 
  }
  printf("\n");
}

uint16_t firstBits(uint32_t x) {
  uint32_t mask = 0xffff0000;
  x &= mask;
  x >>= 16;
  uint16_t y = (uint16_t) x;
  return y;
}

uint16_t secondBits(uint32_t x) {
  uint32_t mask = 0xffff;
  x &= mask;
  uint16_t y = (uint16_t) x;
  return y;
}

int main(void) { 
  uint32_t n = 1288243249; 
  printBits(n); 
  uint16_t first = firstBits(n);
  uint16_t second = secondBits(n);
  printBits16(first);
  printBits16(second);
  int16_t signedFirst = (int16_t) first;
  printf("%i\n%i\n", first, signedFirst);
  printBits16(first);
  printSBits16(signedFirst);
  return 0;
}
