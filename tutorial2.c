#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

char *createCommand(size_t size);
char **createCommands(int n);
char *getCommand(size_t size);
char **getCommands(int n, size_t size);
void printCommands(char **commands, int n);
void freeCommands(char **commands, int n);

// extension functions
int stringLength(char *string);
char *stringCat(char *string1, char *string2);
void printCommandLengths(char **commands, int n);
void printCommandConcat(char **commands, int n);

int main(int argc, char **argv) {
  assert (argc > 2);
  int n = atoi(argv[1]);
  size_t size = atoi(argv[2]);
  assert (n >= 3 && size >= 50);
  char **commands = getCommands(n, size);
  printCommands(commands, n);
  printCommandLengths(commands, n);
  printCommandConcat(commands, n);
  freeCommands(commands, n);
  return 0;
}

char *createCommand(size_t size) {
  char *command = malloc(size * sizeof(char));
  return command;
}

char **createCommands(int n) {
  char **commands = malloc(n * sizeof(char*));
  return commands;
}

char *getCommand(size_t size) {
  printf("> ");
  char *command = createCommand(size);
  int strSize = size * sizeof(char);
  fgets(command, strSize, stdin);
  return command;
}

char **getCommands(int n, size_t size) {
  char **commands = createCommands(n);
  for (int i = 0; i < n; i++) {
    commands[i] = getCommand(size);
  }
  return commands;
}

void printCommands(char **commands, int n) {
  for (int i = 0; i < n; i++) {
    printf("%s", commands[i]);
  }
}

void freeCommands(char **commands, int n) {
  for (int i = 0; i < n; i++) {
    free(commands[i]);
  }
  free(commands);
}

int stringLength(char *string) {
  int i = 0;
  do {
    i++;
  } while (string[i] != '\0');
  return i;
}

char *stringCat(char *string1, char *string2) {
  int length1 = stringLength(string1);
  int length2 = stringLength(string2);
  char *string = malloc((length1 + length2 - 1) * sizeof(char));
  for (int i = 0; i < length1 - 1; i++) {
    string[i] = string1[i];
  }
  for (int i = 0; i < length2; i++) {
    string[i + (length1 - 1)] = string2[i];
  }
  return string;
}

void printCommandLengths(char **commands, int n) {
  for (int i = 0; i < n; i++) {
    printf("%s%i\n", commands[i], stringLength(commands[i]));
  }
}

void printCommandConcat(char **commands, int n) {
  char *string = malloc(sizeof(char));
  for (int i = 0; i < n; i++) {
    char* newString = stringCat(string, commands[i]);
    free(string);
    string = newString;
  }
  printf("%s", string);
}
