#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

struct person {
  char name[50];
  int age;
};

struct person *getPerson(void) {
  struct person *p = malloc(sizeof(struct person));
  printf("Enter the person's name and age: \n");
  char buf[53];
  int bufSize = sizeof(buf);
  fgets(buf, bufSize, stdin);
  int i = 0;
  while (i < 50) {
    if (buf[i] == ' ') {
      break;
    }
    p->name[i] = buf[i];
    i++;
  }
  p->age = atoi(&buf[i + 1]);
  return p;
}

void printPeople(struct person **people, int n) {
  for (int i = 0; i < n; i++) {
    printf("Name: %s\nAge: %i\n", people[i]->name, people[i]->age);
  }
}

int main() {
  int n = 3;
  struct person **people = malloc(n * sizeof(struct person*));
  for (int i = 0; i < n; i++) {
    people[i] = getPerson();
  }
  printPeople(people, n);
  for (int i = 0; i < n; i++) {
    free(people[i]);
  }
  free(people);
  return 0;
}
